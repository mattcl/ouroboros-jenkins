#
# Cookbook Name:: ouroboros-jenkins
# Recipe:: container_builder
#

include_recipe 'ouroboros-jenkins::slave'
include_recipe 'sbp_packer::install_binary'

cookbook_file '/usr/local/bin/packer-post-processor-docker-dockerfile' do
  source 'packer-post-processor-docker-dockerfile'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

dpkg_package 'chefdk' do
  source '/tmp/chefdk.deb'
  action :nothing # this is installed if the remote_file triggers
end

remote_file 'chefdk-deb' do
  path '/tmp/chefdk.deb'
  source node['oj']['jenkins']['chefdk_url']
  notifies :install, 'dpkg_package[chefdk]', :immediately
  not_if "dpkg-query -W 'chefdk'"
end

docker_installation_script 'default' do
  repo 'main'
  script_url 'https://get.docker.com/'
  action :create
end

group 'docker' do
  action :modify
  members node['oj']['jenkins']['user']['name']
  append true
end
