#
# Cookbook Name:: ouroboros-jenkins
# Recipe:: master
#

include_recipe 'ouroboros-jenkins::base'

include_recipe 'jenkins::master'

node['oj']['jenkins']['plugins']['installed'].each do |plugin, version|
  jenkins_plugin plugin do
    version version if version
  end
end

jenkins_command 'safe-restart'
