#
# Cookbook Name:: ouroboros-jenkins
# Recipe:: base
#

node.set['java']['jdk_version'] = '7'

include_recipe 'apt'
include_recipe 'java'
include_recipe 'git'

jenkins_home = node['oj']['jenkins']['user']['home']

group node['oj']['jenkins']['group']['name'] do
  gid node['oj']['jenkins']['group']['gid']
end

user node['oj']['jenkins']['user']['name'] do
  comment 'The jenkins user'
  uid node['oj']['jenkins']['user']['uid']
  gid node['oj']['jenkins']['group']['gid']
  home jenkins_home
  password node['oj']['jenkins']['user']['password']
  shell '/bin/bash'
end

directory jenkins_home do
  owner node['oj']['jenkins']['user']['name']
  group node['oj']['jenkins']['group']['name']
end
