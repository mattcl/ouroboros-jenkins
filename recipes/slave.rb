#
# Cookbook Name:: ouroboros-jenkins
# Recipe:: slave
#

include_recipe 'ouroboros-jenkins::base'

cookbook_file File.join(node['oj']['jenkins']['user']['home'],
                        'swarm-client.jar') do
  source 'swarm-client-1.26-jar-with-dependencies.jar'
  owner node['oj']['jenkins']['user']['name']
  group node['oj']['jenkins']['group']['name']
  mode '0755'
  action :create
end
