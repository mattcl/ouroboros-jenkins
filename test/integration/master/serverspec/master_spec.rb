require 'spec_helper'
require 'shared_examples/base_box'

describe 'Jenkins Master' do
  it_behaves_like 'a jenkins machine'

  describe service('jenkins') do
    it { is_expected.to be_enabled }
    it { is_expected.to be_running }
  end
end
