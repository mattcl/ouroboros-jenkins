# vim: set ft=rspec:

shared_examples 'a jenkins machine' do
  describe group('jenkins') do
    it { is_expected.to exist }
    it { is_expected.to have_gid(1000) }
  end

  describe user('jenkins') do
    it { is_expected.to exist }
    it { is_expected.to belong_to_group('jenkins') }
    it { is_expected.to have_uid(1000) }
    it { is_expected.to have_login_shell('/bin/bash') }
    it { is_expected.to have_home_directory('/var/jenkins_home') }
    its(:encrypted_password) {
      is_expected.to eq(
        '$6$WNOFtDnmi$ROAqD.pqvxBkwz7uhGQK7o5wQnBSqlzGHGRdFkpMKLV6AkDcs.Ry3N5D4RjaaW7JgjvW7HCkU/5mwEtoeNQCe0' # rubocop:disable Metrics/LineLength
      )
    }
  end

  describe file('/var/jenkins_home') do
    it { is_expected.to exist }
    it { is_expected.to be_directory }
    it { is_expected.to be_owned_by('jenkins') }
    it { is_expected.to be_grouped_into('jenkins') }
  end

  describe command('java -version') do
    # Of course java prints the version to stderr...
    its(:stderr) { is_expected.to include('java version "1.7') }
  end

  describe package('git') do
    it { is_expected.to be_installed }
  end
end
