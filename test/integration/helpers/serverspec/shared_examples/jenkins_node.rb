# vim: set ft=rspec:

shared_examples 'a jenkins node' do
  describe file('/var/jenkins_home/swarm-client.jar') do
    it { is_expected.to exist }
    it { is_expected.to be_file }
    it { is_expected.to be_owned_by('jenkins') }
    it { is_expected.to be_grouped_into('jenkins') }
  end
end
