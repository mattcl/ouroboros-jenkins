require 'spec_helper'
require 'shared_examples/base_box'
require 'shared_examples/jenkins_node'

describe 'Jenkins Slave' do
  it_behaves_like 'a jenkins machine'
  it_behaves_like 'a jenkins node'
end
