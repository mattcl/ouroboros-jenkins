require 'spec_helper'
require 'shared_examples/base_box'
require 'shared_examples/jenkins_node'

describe 'Jenkins Container Builder' do
  it_behaves_like 'a jenkins machine'
  it_behaves_like 'a jenkins node'

  describe package('chefdk') do
    it { is_expected.to be_installed }
  end

  it 'has packer installed' do
    expect(command('which packer').exit_status).to be(0)
  end

  it 'has the dockerfile postprocessor installed' do
    expect(
      command('which packer-post-processor-docker-dockerfile').exit_status
    ).to be(0)
  end

  describe 'docker' do
    let(:docker_cmd) { command('/usr/bin/docker --version') }

    it 'is installed' do
      expect(docker_cmd.exit_status).to be(0)
    end
  end

  describe user('jenkins') do
    it { is_expected.to belong_to_group('docker') }
  end
end
