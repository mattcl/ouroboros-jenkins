default['oj']['jenkins']['user']['name'] = 'jenkins'
default['oj']['jenkins']['user']['uid'] = 1000
default['oj']['jenkins']['user']['home'] = '/var/jenkins_home'
default['oj']['jenkins']['user']['password'] = '$6$WNOFtDnmi$ROAqD.pqvxBkwz7uhGQK7o5wQnBSqlzGHGRdFkpMKLV6AkDcs.Ry3N5D4RjaaW7JgjvW7HCkU/5mwEtoeNQCe0' # rubocop:disable Metrics/LineLength
default['oj']['jenkins']['group']['name'] = 'jenkins'
default['oj']['jenkins']['group']['gid'] = 1000
default['oj']['jenkins']['chefdk_url'] = 'https://packages.chef.io/stable/ubuntu/12.04/chefdk_0.12.0-1_amd64.deb' # rubocop:disable Metrics/LineLength

default['oj']['jenkins']['plugins']['installed'] = {
  'git'                 => nil,
  'greenballs'          => nil,
  'jobConfigHistory'    => nil,
  'swarm'               => nil,
  'workflow-aggregator' => nil,
}
